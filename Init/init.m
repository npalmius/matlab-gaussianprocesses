function [] = init
% Copyright (c) 2016, Nick Palmius (University of Oxford)
% All rights reserved.
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are
% met:
%
% 1. Redistributions of source code must retain the above copyright
%    notice, this list of conditions and the following disclaimer.
%
% 2. Redistributions in binary form must reproduce the above copyright
%    notice, this list of conditions and the following disclaimer in the
%    documentation and/or other materials provided with the distribution.
%
% 3. Neither the name of the University of Oxford nor the names of its
%    contributors may be used to endorse or promote products derived
%    from this software without specific prior written permission.
%
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
% HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
% LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
% DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
% THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

% Contact: npalmius@googlemail.com
% Originally written by Nick Palmius, 27-Nov-2016

    gp_root_path = fileparts(fileparts(mfilename('fullpath')));
    gpml_path = [gp_root_path '/gpml'];
    
    d = dir([gpml_path '/gpml-matlab-*']);
    
    versions = cell(numel(d), 1);
    
    j = 1;
    
    for i = 1:numel(d)
        if d(i).isdir
            versions{j} = d(i).name;
            j = j + 1;
        end
    end
    
    version_to_load = [];
    
    if j > 1
        versions = sort(versions(1:(j - 1)));
        version_to_load = versions{end};
    end
    
    if ~isempty(version_to_load)
        fprintf('Loading %s: ', version_to_load);
        full_path = fullfile([gpml_path '/' version_to_load]);
        if ~isempty(strfind(path, [full_path pathsep]))
            fprintf('already loaded!\n');
        else
            run([gpml_path '/' version_to_load '/startup.m'])
        end
    else
        fprintf('GPML not found!\n');
    end
end
