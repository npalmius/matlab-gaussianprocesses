#!/bin/bash

DIR=$( cd "$(dirname "$0")" ; pwd - P )

temp_dir="$DIR/.temp"

echo $temp_dir

if [ ! -d "$temp_dir" ]; then
  mkdir "$temp_dir"
fi

gpmlversion="gpml-matlab-v4.0-2016-10-19"

wget -nc "http://gaussianprocess.org/gpml/code/matlab/release/$gpmlversion.zip" -O "$temp_dir/$gpmlversion.zip"

gpml_dir="$DIR/gpml"

if [ ! -d "$gpml_dir" ]; then
  mkdir "$gpml_dir"
fi

unzip "$temp_dir/$gpmlversion.zip" -d "$gpml_dir"
